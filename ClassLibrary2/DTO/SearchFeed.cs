﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;
using Nest;



namespace ElasticSearchSettings.DTO
{

    [ElasticsearchType(IdProperty = "Id", Name = "post")]       // 2. [*] use local 'Id' to calculate the value of "primary key" i.e. '_id' for each elastic search document
    public class SearchFeed
    {
        [IgnoreAttribute]       // 1. Ignore during auto-mapping but [*]
        public string Id
        {
            get
            {
                return Util.CreateMD5(Title + Uri + PostDate.ToUniversalTime().ToString());
            }
        }


        public string Title { get; set; }
        public DateTime PostDate { get; set; }
        public string Uri { get; set; }
        public string Description { get; set; }

        public SearchFeed()
        {

        }

        public SearchFeed(SourceFeed source)
        {
            Title = source.Title;
            PostDate = source.PublishedDate;
            Uri = source.Link;
            Description = source.Description;
        }
    }
}
