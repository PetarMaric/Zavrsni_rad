﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;



namespace ElasticSearchSettings.DTO
{
    public class SourceFeed
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public DateTime PublishedDate { get; set; }
        public string Description { get; set; }

        public SourceFeed(XElement item)
        {
            Title = item.Element("title").Value;
            Link = item.Element("link").Value;
            PublishedDate = DateTime.Parse(item.Element("pubDate").Value, null, DateTimeStyles.AdjustToUniversal);
            Description = item.Element("description").Value;
        }
    }
}
