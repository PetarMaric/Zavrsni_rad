﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchSettings.DTO;
using System.Xml.Linq;

namespace ElasticSearchSettings
{
    public static class RssReader
    {
        public static IEnumerable<SourceFeed> ReadFeed(string url)
        {
            XElement xmlFeed = XElement.Load(url);
            IEnumerable<SourceFeed> items = from item in xmlFeed.Elements("channel").Elements("item")
                                            select new SourceFeed(item);
            return items;
        }
    }
}
