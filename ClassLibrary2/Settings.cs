﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using ElasticSearchSettings.DTO;
using System.Configuration;



namespace ElasticSearchSettings
{
    public class Settings
    {
        private static ElasticClient _client = null;
        private static string _elasticIndexName;
        static Settings()
        {
            string elasticUrl = ConfigurationManager.AppSettings["ElasticUrl"].ToString();
            _elasticIndexName = ConfigurationManager.AppSettings["ElasticIndexName"].ToString();



            Uri node = new Uri(elasticUrl);
            ConnectionSettings settings = new ConnectionSettings(node)
                    .DefaultIndex(_elasticIndexName)
                    .PrettyJson()
                    .DisableDirectStreaming();
            _client = new ElasticClient(settings);
        }

        public static bool DeleteIndex(string indexName)
        {
            try
            {
                if (_client != null && _client.IndexExists(indexName).Exists == true)
                {
                    _client.DeleteIndex(indexName);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        // https://www.elastic.co/guide/en/elasticsearch/client/net-api/current/fluent-mapping.html#_manual_mapping
        // https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-get-mapping.html
        // https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-analyzers.html

        public static bool CreateIndex<T>(string indexName) where T : class
        {
            try
            {
                if (_client != null && _client.IndexExists(indexName).Exists == false)
                {
                    _client.CreateIndex(indexName, c => c
                         .Settings(s => s
                                 .NumberOfShards(1)
                                 .NumberOfReplicas(0))
                         .Mappings(m => m
                                 .Map<SearchFeed>(mp => mp
                                        .AutoMap()
                                        .Properties(ps=>ps
                                            .Text(s=>s
                                                .Name(e=>e.Title)
                                                     .Analyzer("standard")
                                        ))
                                        .Properties(px=>px
                                            .Text(s1=>s1
                                                .Name(f=>f.Description)
                                                    .Analyzer("standard")))
                                         .Properties(py=>py
                                            .Text(s2=>s2
                                                .Name(g=>g.Uri)
                                                    .Analyzer("keyword"))))));

                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool StoreParsedFeed(SearchFeed post)
        {
            try
            {
                _client.IndexDocument(post);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        // Storing feeds
        public static IBulkResponse StoreParsedFeeds(IEnumerable<SearchFeed> posts)
        {
            Nest.IBulkResponse response = null;
            try
            {
                response = _client.IndexMany<SearchFeed>(posts, _elasticIndexName);
                return response;
            }
            catch (Exception ex)
            {
                return response;
            }
        }


        // MatchPhrase
        public static List<SearchFeed> SearchByTitleMatchPhrase(string keyword, int size)
        {
            var result = new List<SearchFeed>();
            try
            {
                var searchResponse = _client.Search<SearchFeed>(s => s
                    .From(0)
                    .Size(size)
                    .Query(q => q
                        .MatchPhrase(m => m
                            .Field(f => f.Title)
                            .Query(keyword)
                            .Slop(3)
                            .Analyzer("standard")
                            )
                        )
                  );

                if (searchResponse != null)
                {
                    var collection = searchResponse.Documents;
                    result.AddRange(collection);
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }


        // MatchAll
        public static List<SearchFeed> SearchByTitleMatchAll(int size)
        {
            var result = new List<SearchFeed>();
            try
            {
                var searchResponse = _client.Search<SearchFeed>(s => s
                    .From(0)
                    .Size(size)
                    .Query(q => q
                        .MatchAll()
                        )
                  );

                if (searchResponse != null)
                {
                    var collection = searchResponse.Documents;
                    result.AddRange(collection);

                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }


        // TitleMatch
        public static List<SearchFeed> SearchByTitleMatch(string keyword, int size)
        {
            var result = new List<SearchFeed>();
            try
            {
                var searchResponse = _client.Search<SearchFeed>(s => s
                    .From(0)
                    .Size(size)
                    .Query(q => q
                        .Match(m => m
                            .Field(f => f.Title)
                            .Query(keyword)
                            .Fuzziness(Fuzziness.Auto)
                            .FuzzyTranspositions(true)          
                            .Analyzer("standard")      
                            )
                        )
                  );

                if (searchResponse != null)
                {
                    var collection = searchResponse.Documents;
                    result.AddRange(collection);
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }


        // MatchPhraseDescription
        public static List<SearchFeed> SearchByDescriptionMatchPhrase(string keyword, int size)
        {
            var result = new List<SearchFeed>();
            try
            {
                var searchResponse = _client.Search<SearchFeed>(s => s
                    .From(0)
                    .Size(size)
                    .Query(q => q
                        .MatchPhrase(m => m
                            .Field(f => f.Description)
                            .Query(keyword)
                            .Slop(4)
                            .Analyzer("standard")
                            )
                        )
                  );

                if (searchResponse != null)
                {
                    var collection = searchResponse.Documents;
                    result.AddRange(collection);
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }


        // DescriptionMatch
        public static List<SearchFeed> SearchByDescriptionMatch(string keyword, int size)
        {
            var result = new List<SearchFeed>();
            try
            {
                var searchResponse = _client.Search<SearchFeed>(s => s
                    .From(0)
                    .Size(size)
                    .Query(q => q
                        .Match(m => m
                            .Field(f => f.Description)
                            .Query(keyword)
                            .Fuzziness(Fuzziness.Auto)
                            .FuzzyTranspositions(true)
                            .Analyzer("standard")
                            )
                        )
                  );

                if (searchResponse != null)
                {
                    var collection = searchResponse.Documents;
                    result.AddRange(collection);
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

    }
}

