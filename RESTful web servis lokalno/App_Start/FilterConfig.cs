﻿using System.Web;
using System.Web.Mvc;

namespace RESTful_web_servis_lokalno
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
