﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ElasticSearchSettings;
using ElasticSearchSettings.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RESTful_web_servis_lokalno.Custom_Classes;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;


namespace RESTful_web_servis_lokalno.Controllers
{
    public class MatchAllController : ApiController
    {


        [HttpGet]
        public JObject GetMatchAll(int size = Constraints.MATCH_ALL_DEFAULT)
        {

            int sizeLocal = size;

            List<SearchFeed> result = new List<SearchFeed>();

            if (size <= Constraints.MATCH_ALL_MAX && size >=Constraints.MATCH_ALL_MIN)
            {
                result = Settings.SearchByTitleMatchAll(sizeLocal);
            }
            else
            {
                return new JObject(new JProperty(Schema.RECEIVED_QUERY, "Match all"),
                                    new JProperty(Schema.WARNING, "Can not execute the query. Please choose the size between " + Constraints.MATCH_ALL_MIN + " and " + Constraints.MATCH_ALL_MAX + "."));
            }

            JObject json = new JObject
            (
                new JProperty(Schema.QUERY, "Match all"),
                new JProperty(Schema.SELECTED_SIZE, sizeLocal),
                new JProperty(Schema.RESULT_COUNT, result.Count),
                new JProperty(Schema.RESULTS, new JArray(
                                                          from p in result
                                                          select new JObject(
                                                                              new JProperty(Schema.TITLE, p.Title),
                                                                              new JProperty(Schema.URI, p.Uri),
                                                                              new JProperty(Schema.POST_DATE, p.PostDate),
                                                                              new JProperty(Schema.DESCRIPTION, p.Description)))));

            return json;
        }
    }
}
