﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ElasticSearchSettings;
using ElasticSearchSettings.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RESTful_web_servis_lokalno.Custom_Classes;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace RESTful_web_servis_lokalno.Controllers
{
    public class UpdateFeedController : ApiController
    {


        [HttpGet]
        public List<BulkResponseClass> GetUpdateFeed()
        {  

            string[] urlFeeds = new string[]
            {
                            "http://feeds.arstechnica.com/arstechnica/index/",
                            "https://www.phoronix.com/rss.php",
                            "http://feed.cnet.com/feed/topics/tech-industry",
                            "http://feeds.feedburner.com/HowToGeek",
                            "http://feeds.nationalgeographic.com/ng/News/News_Main"

            };

            int urlFeedsLength = urlFeeds.Length;

            List<BulkResponseClass> ListOfInsertedItems = new List<BulkResponseClass>();

            foreach (var url in urlFeeds)
            {      
                
                var feeds = RssReader.ReadFeed(url);
                if (feeds != null)
                {
                    feeds = feeds.ToList();
                    Nest.IBulkResponse response = Settings.StoreParsedFeeds(feeds.Select(x => new SearchFeed(x)).Where(x => x != null).ToList());

                    ListOfInsertedItems.Add(new BulkResponseClass(response.ApiCall.HttpMethod, response.ApiCall.HttpStatusCode.ToString(), response.ApiCall.ResponseMimeType, response.ApiCall.Success, response.ApiCall.Uri.AbsoluteUri, url));
                }


            }
           
            return ListOfInsertedItems;
        }


    }
}


           

