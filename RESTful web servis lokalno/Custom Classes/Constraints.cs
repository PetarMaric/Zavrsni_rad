﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTful_web_servis_lokalno.Custom_Classes
{
    public class Constraints
    {
        // MATCH_ALL CONSTRAINTS
        public const int MATCH_ALL_DEFAULT = 70;
        public const int MATCH_ALL_MIN = 1;
        public const int MATCH_ALL_MAX = 500;

        // MATCH_PHRASE CONSTRAINTS
        public const int MATCH_PHRASE_DEFAULT = 30;
        public const int MATCH_PHRASE_MIN = 1;
        public const int MATCH_PHRASE_MAX = 50;

        // MATCH_TITLE CONSTRAINTS
        public const int MATCH_TITLE_DEFAULT = 30;
        public const int MATCH_TITLE_MIN = 1;
        public const int MATCH_TITLE_MAX = 50;

        // MATCH_PHRASE_DESCRIPTION CONSTRAINTS
        public const int MATCH_PHRASE_DESCRIPTION_DEFAULT = 30;
        public const int MATCH_PHRASE_DESCRIPTION_MIN = 1;
        public const int MATCH_PHRASE_DESCRIPTION_MAX = 50;

        // MATCH_TITLE_DESCRIPTION CONSTRAINTS
        public const int MATCH_DESCRIPTION_DEFAULT = 30;
        public const int MATCH_DESCRIPTION_MIN = 1;
        public const int MATCH_DESCRIPTION_MAX = 50;

    }
}