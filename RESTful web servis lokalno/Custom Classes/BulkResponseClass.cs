﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nest;
using Newtonsoft.Json;

namespace RESTful_web_servis_lokalno.Custom_Classes
{
    public class BulkResponseClass
    {

        [JsonIgnore]
        public bool IsValid { get; set; }

        public string HttpMethod { get; set; }
        public string HttpStatusCode { get; set; }
        public string ResponseMimeType { get; set; }
        public bool Success { get; set; }
        public string Uri { get; set; }
        public string FeedUri { get; set; }


        public BulkResponseClass(Elasticsearch.Net.HttpMethod HttpMethod, string HttpStatusCode, string ResponseMimeType, bool Success, string Uri, string FeedUri)
        {
            this.HttpMethod = HttpMethod.ToString();
            this.HttpStatusCode = HttpStatusCode;
            this.ResponseMimeType = ResponseMimeType;
            this.Success = Success;
            this.Uri = Uri;
            this.FeedUri = FeedUri;
        }

        public BulkResponseClass(bool Success)
        {
            this.Success = Success;
        }
    }
}
