﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTful_web_servis_lokalno.Custom_Classes
{
    public class Schema
    {
        public const string RECEIVED_QUERY = "ReceivedQuery";
        public const string WARNING = "Warning";
        public const string QUERY = "Query";
        public const string RESULT_COUNT = "ResultCount";
        public const string RESULTS = "Results";
        public const string SELECTED_SIZE = "SelectedSize";

        public const string TITLE = "title";
        public const string URI = "Uri";
        public const string POST_DATE = "PostDate";
        public const string DESCRIPTION = "Description";
    }
}